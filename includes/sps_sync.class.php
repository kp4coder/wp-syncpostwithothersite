<?php
if( !class_exists ( 'SPS_Sync' ) ) {

    class SPS_Sync {
        var $is_website_post = true;
        var $post_old_title = '';

        function __construct(){
            
            add_filter( 'wp_insert_post_data' , array( $this, 'filter_post_data') , '99', 2 );

            add_action( "save_post", array( $this, "sps_save_post" ), 10 , 3 );

            add_action( "init", array( $this, "sps_get_request" ) );

        }


        function filter_post_data( $data , $postarr ) {
            global $post_old_title;
            if( isset($postarr['ID']) && !empty($postarr['ID']) ) {
                $old_data = get_posts( array( 'ID' => $postarr['ID'] ) );
                if( $old_data && isset($old_data[0]->post_title) && $postarr != $old_data[0]->post_title ) {
                    $post_old_title = $old_data[0]->post_title; 
                } 
            }

            return $data;
        }

        function sps_save_post( $post_ID, $post, $update ) {         

            $sps_website = isset($_REQUEST['sps_website']) ? $_REQUEST['sps_website'] : array();
            $status_not = array('auto-draft', 'trash', 'inherit', 'draft');
            if($this->is_website_post && isset($post->post_status) && !in_array($post->post_status, $status_not) && !empty($sps_website) ) {

                global $wpdb, $sps, $sps_settings, $post_old_title;
                $general_option = $sps_settings->sps_get_settings_func();

                if( !empty( $general_option ) && isset( $general_option['sps_host_name'] ) && !empty( $general_option['sps_host_name'] ) ) {
                    
                    foreach ($general_option['sps_host_name'] as $sps_key => $sps_value) { 
                        $args = (array) $post;

                        $args['sps']['host_name']   = !empty( $sps_value ) ? $sps_value : '';
                        $args['sps']['strict_mode'] = isset( $general_option['sps_strict_mode'][$sps_key] ) ? $general_option['sps_strict_mode'][$sps_key] : 1;
                        $args['sps']['roles'] = isset( $general_option['sps_roles_allowed'][$sps_key]['roles'] ) ? $general_option['sps_roles_allowed'][$sps_key]['roles'] : array();
                        $args['sps']['roles']['administrator'] = 'on';

                        $args['sps']['content_match'] = isset( $general_option['sps_content_match'][$sps_key] ) ? $general_option['sps_content_match'][$sps_key] : 'title';
                        $args['sps']['content_username'] = isset( $general_option['sps_content_username'][$sps_key] ) ? $general_option['sps_content_username'][$sps_key] : '';
                        $args['sps']['content_password'] = isset( $general_option['sps_content_password'][$sps_key] ) ? $general_option['sps_content_password'][$sps_key] : '';

                        if( !empty($post_old_title) ) {
                            $args['post_old_title'] = $post_old_title;
                        } else {
                            $args['post_old_title'] = $args['post_title'];
                        }

                        if( has_post_thumbnail($post_ID) ) {
                           $args['featured_image'] = get_the_post_thumbnail_url($post_ID);
                        }

                        if( isset($args['post_content']) && !( isset($args['sps']['strict_mode']) && $args['sps']['strict_mode'] ) ) {
                            $args['post_content'] = strip_tags($args['post_content']);
                        }

                        $loggedin_user_role = wp_get_current_user();
                        $matched_role = array_intersect( $loggedin_user_role->roles, array_keys( $args['sps']['roles'] ) );

                        if( !empty($sps_value) && !empty($matched_role) && in_array($sps_value, $sps_website) ) {

                            $post_metas = get_post_meta($post_ID);
                            if( !empty($post_metas) ) {
                                foreach ( $post_metas as $meta_key => $meta_value ) {
                                    if( $meta_key != 'sps_website' ) {
                                        $args['meta'][$meta_key] = isset($meta_value['0']) ? $meta_value['0'] : '';
                                    }
                                }
                            }

                            $url = $sps_value."/?sps_post=".$post_ID;

                            $response = wp_remote_post( $url, array( 'body' => $args ));

                            if( isset( $response['response']['code'] ) && $response['response']['code'] == 200 ) {
                                $other_site_post_id = $response['body'];
                            }
                        }                       
                    } 
                } 
            }
        }

        function sps_check_data( $content_mach, $post_data ) {

            global $wpdb;

            $the_slug = $post_data['post_name'];
            $the_title = isset( $post_data['post_old_title'] ) ? $post_data['post_old_title'] : '';

            $args_title = array(
              'title'        => $the_title,
              'post_type'   => 'post'
            );
            $args_slug = array(
              'name'        => $the_slug,
              'post_type'   => 'post'
            );
          
            $post_id = '';
            if($content_mach=="title") {
                $my_posts = get_posts($args_title);
                if($my_posts) { 
                    $post_id = $my_posts[0]->ID; 
                }
            } else if($content_mach=="title-slug") {
                $my_posts = get_posts($args_title);
                if($my_posts) {
                    $post_id = $my_posts[0]->ID; 
                } else { 
                    $my_posts2 = get_posts($args_slug);
                    if($my_posts2) { 
                        $post_id = $my_posts2[0]->ID; 
                    }
                }
            } else if($content_mach=="slug") {
                $my_posts = get_posts($args_slug);
                if($my_posts) { 
                    $post_id = $my_posts[0]->ID; 
                }
            } else if($content_mach=="slug-title") {
                $my_posts = get_posts($args_slug);
                if($my_posts) {
                    $post_id = $my_posts[0]->ID; 
                } else {
                    $my_posts = get_posts($args_title);
                    if($my_posts) {
                        $post_id = $my_posts[0]->ID; 
                    }
                }
            }

            return $post_id;
        }

        function grab_image($url,$saveto){

            $data = wp_remote_request( $url );
            
            if( isset( $data['body'] ) && isset( $data['response']['code'] ) && !empty( $data['response']['code'] ) ) {
                $raw = $data['body'];
                if(file_exists($saveto)){
                    unlink($saveto);
                }
                $fp = fopen($saveto,'x');
                fwrite($fp, $raw);
                fclose($fp);
            }
        }

        function sps_get_request() {

            if( isset($_REQUEST['sps_post']) && !empty($_REQUEST['sps_post']) ) {
                $this->is_website_post = false;

                $sps_sync_data = $_REQUEST;
                
                // global $sps;
                // $sps->sps_write_log(json_encode( sanitize_text_field($sps_sync_data) ));

                $sps_host_name        = isset($sps_sync_data['sps']['host_name']) ? esc_url_raw($sps_sync_data['sps']['host_name']) : '';
                $sps_strict_mode      = isset($sps_sync_data['sps']['strict_mode']) ? sanitize_text_field($sps_sync_data['sps']['strict_mode']) : '';
                $sps_content_match    = isset($sps_sync_data['sps']['content_match']) ? sanitize_text_field($sps_sync_data['sps']['content_match']) : '';
                $sps_roles            = isset($sps_sync_data['sps']['roles']) ? sanitize_text_field($sps_sync_data['sps']['roles']) : '';
                $sps_content_username = isset($sps_sync_data['sps']['content_username']) ? sanitize_text_field($sps_sync_data['sps']['content_username']) : '';
                $sps_content_password = isset($sps_sync_data['sps']['content_password']) ? sanitize_text_field($sps_sync_data['sps']['content_password']) : '';

                unset($sps_sync_data['sps']);
                if( isset($sps_sync_data['ID']) ) {
                    unset($sps_sync_data['ID']);
                }

                $return = array();
                if( !empty($sps_content_username) && !empty($sps_content_password) ) {
                    $auther = wp_authenticate( $sps_content_username, $sps_content_password );

                    if( isset($auther->ID) && !empty($auther->ID) ) {
                        
                        $sps_sync_data['post_author'] = $auther->ID;
                        $post_id = $this->sps_check_data( $sps_content_match, $sps_sync_data );

                        $post_action = '';
                        if( !empty($post_id) ) {
                            $post_action = 'edit';
                            $sps_sync_data['ID'] = $post_id;
                            $post_id = wp_update_post( $sps_sync_data );
                        } else {
                            $post_action = 'add';
                            $post_id = wp_insert_post( $sps_sync_data );
                        }

                        if( isset($sps_sync_data['featured_image']) && !empty($sps_sync_data['featured_image']) ) {
                            
                            $image_url        = $sps_sync_data['featured_image'];
                            $image_name       = end( explode( '/', $sps_sync_data['featured_image'] ) );
                            $upload_dir       = wp_upload_dir();
                            $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name );
                            $filename         = basename( $unique_file_name );

                            // Check folder permission and define file location
                            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                                $file = $upload_dir['path'] . '/' . $filename;
                            } else {
                                $file = $upload_dir['basedir'] . '/' . $filename;
                            }

                            // Create the image  file on the server
                            $this->grab_image( $image_url, $file);

                            // Check image file type
                            $wp_filetype = wp_check_filetype( $filename, null );

                            // Set attachment data
                            $attachment = array(
                                'post_mime_type' => $wp_filetype['type'],
                                'post_title'     => sanitize_file_name( $filename ),
                                'post_content'   => '',
                                'post_status'    => 'inherit'
                            );

                            // Create the attachment
                            $attach_id = wp_insert_attachment( $attachment, $file, $post_id );

                            // Include image.php
                            require_once(ABSPATH . 'wp-admin/includes/image.php');

                            // Define attachment metadata
                            $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

                            // Assign metadata to attachment
                            wp_update_attachment_metadata( $attach_id, $attach_data );

                            // And finally assign featured image to post
                            set_post_thumbnail( $post_id, $attach_id );
                        }

                        if( isset($sps_sync_data['meta']) && !empty($sps_sync_data['meta']) ) {
                            foreach ($sps_sync_data['meta'] as $meta_key => $meta_value) {
                                add_post_meta( $post_id, $meta_key, $meta_value );
                            }
                        }

                        $return['status'] = __('success', SPS_txt_domain);
                        $return['msg'] = __('data proccessed successfully', SPS_txt_domain);
                        $return['post_id'] = $post_id;
                        $return['post_action'] = $post_action;

                    } else {
                        $return['status'] = __('failed', SPS_txt_domain);
                        $return['msg'] = __('Authenitcate failed.', SPS_txt_domain);
                    }
                } else {
                    $return['status'] = __('failed', SPS_txt_domain);
                    $return['msg'] = __('Username or Password is null.', SPS_txt_domain);
                }
                echo json_encode( $return );
                exit;
            }
        }
    }

    global $sps_sync, $post_old_title;
    $sps_sync = new SPS_Sync();

}

?>