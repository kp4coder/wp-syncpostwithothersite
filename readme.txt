=== Sync Post With Other Site ===
Contributors: kp4coder
Donate link: https://kp4coder.com/plugin-syncpostwithothersite/
Tags: sync post content, sync post with multiple sites, post attachments, post content, post content sync, migrate post content, moving post data, synchronization post
Requires at least: 4.5
Tested up to: 5.0.0
Stable tag: 1.2.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows user to sync post with multiple websites.

== Description ==

Allows user to sync post with multiple websites. You just need to enter website URL & login credentials of other website to sync the post from there.

== Installation ==

Install Via Wordpress Uploader : In your WordPress admin, go to Plugins > Add New > Upload and upload the available ZIP of Sync Post With Other Site to install the Plugin.

Manual Installation :
Download the latest version of the Sync Post With Other Site.
Unzip the downloaded file to your computer.
Upload the /SyncPostWithOtherSite/ directory to the /wp-content/plugins/ directory of your site.
Activate the plugin through the ‘Plugins’ menu in WordPress.

== Frequently asked questions ==

= Do I need to Install WPSiteSync for Content on both sites? =

Yes! The WPSiteSync for Content needs to be installed on the local or Staging server (the website you're moving the data from - the Source), as well as the Live server (the website you're moving the data to - the Target).


== Screenshots ==
1. Setting Page
2. Post Add / Edit Select Website.

== Upgrade notice ==

First release.